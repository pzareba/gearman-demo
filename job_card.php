<?php
class JobCard
{
    // property declaration
    private $jobID = "";
    private $fileName = "";
    private $status = "NewJob";
    private $result = "";

    // method declaration
    public function getJobID()   { return $this->jobID; }
    public function getFileName(){ return $this->fileName; }
    public function getStatus()  { return $this->status; }
    public function getResult()  { return $this->result; }

    public function setJobID( $server, $jobNumber ) { 

        $this->jobID = $server . "::" . $jobNumber; 
    }
    public function setFileName( $name ) { $this->fileName = $name; }
    public function setStatus( $status ) { $this->status = $status; }
    public function setResult( $result ) { $this->result = $result; }
}
?>
