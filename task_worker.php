<?php
require('job_card.php');

define ("MAX_TASKS", 5);


$worker= new GearmanWorker();
$worker->addServer();
$worker->addFunction("extract", "extract_function");
$worker->addFunction("reverse", "reverse_function");
$worker->addFunction("capitalise", "capitalise_function");

//
// Process a task.
//
// A task is an individual item of work within a job card.
// 
$tasksComplete = 0;
do {
    printf( "    [Task-%d] WAIT\n", $tasksComplete+1 );

    $worker->work();
    $return = $worker->returnCode();
    $tasksComplete++;

    if ($return != GEARMAN_SUCCESS) {
        printf( "ERROR: %d\n", $worker->returnCode() );
    }

} while($return == GEARMAN_SUCCESS && $tasksComplete < MAX_TASKS);

printf( "WORKER SIGNING OFF\n");


function extract_function($job)
{
//print_r( $job );

    // Reform the JobCard object.
    $workload = $job->workload();
    $jobCard = unserialize(json_decode($workload));

    printf( "    [Task] EXTRACT    : - %s\n", $jobCard->getFileName() );

    return strtoupper($job->workload());
}

function capitalise_function($job)
{
    // Reform the JobCard object.
    $workload = $job->workload();
    $jobCard = unserialize(json_decode($workload));

    printf( "    [Task] CAPITIALISE: - %s\n", $jobCard->getFileName() );

    return strtoupper($job->workload());
}

function reverse_function($job)
{
    // Reform the JobCard object.
    $workload = $job->workload();
    $jobCard = unserialize(json_decode($workload));

    printf( "    [Task] REVERSE    : - %s\n", $jobCard->getFileName() );

    return strrev($job->workload());
}
?>
