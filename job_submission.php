<?php

require('job_card.php');

define( "PROCESS_LIST", "SFA-ProcessList");

define( "ERROR_WAIT", 5);
define( "MAX_RETRY", 5);

// Determine which queue to listen on.
$queueName = PROCESS_LIST;
$errorCount = 0;


//Connecting to Redis server on localhost
$redis = new Redis();
if( $redis->connect('127.0.0.1', 6379) !== true ) {

        // some other code to handle connection problem
        die( "Cannot connect to redis server.\n" );
}

// Read off job cards as they arise.
$redis->setOption(Redis::OPT_READ_TIMEOUT, -1);
while ( true ) {

    try {
        printf( "  [%s] WAIT\n", $queueName );
        $request = $redis->blPop($queueName, 0);

	if( count($request) > 1 ) {
        printf( "  [%s] RECV\n", $queueName );

	// Call succeded - reset error count.
        $errorCount = 0;

        // Convert payload back into a JobCard object.
        $jobCard   = $request[1];
        $jobCard = unserialize(json_decode($jobCard));

        schedule_job( $queueName, $jobCard );
        }
    }
    catch( RedisException $e ) {

        printf( "\nERROR: Caught exception\n" );
        printf( "Exception text - '%s':\n", $e->getMessage() );
        if ( $errorCount > MAX_RETRY ) {
            printf( "Exceeded retry count... Aborting!\n" );
            exit( 1 );
        } else {
            $errorCount++;
            printf( "Retrying n %d seconds...\n\n", ERROR_WAIT );
            sleep( ERROR_WAIT );
        }
    }

} 

function schedule_job( $queueName, $jobCard ) {

    printf( "  [%s] SCHED JOB: <%s>\n", $queueName, 
            $jobCard->getJobID(), $jobCard->getFileName() );

    $client= new GearmanClient();
    $client->addServer();
    $client->setCreatedCallback("jobcard_created");

    $json_string = json_encode(serialize($jobCard));

    // 
    // Submit a job card for execution.
    // 
    // The theory is that job cards are submitted asynchronously.
    // The idea being that gearman will load balance across its servers.
    // 
    // Each job card will have a number of tasks.
    // Once a job card is started the task will need to be
    // executed synchronously by the workers.
    //
    $job = $client->addTask("jobcard", $json_string);

    if (! $client->runTasks())
    {
        echo "ERROR " . $gmc->error() . "\n";
        exit;
    }
}


function jobcard_created($task)
{
    printf( "    [JobCard] JOB CREATED: %s\n", $task->jobHandle() );
}

?>

